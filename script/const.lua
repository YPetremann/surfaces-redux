--[[
Surfaces (Factorio Mod)
Copyright (C) 2019 Simon Crawley, DeltaNedas & Daan Vanoverloop

This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
]]

require("config")
require("script.lib.util-base")

-- Note: This is NOT a configuration file! Data in this file CAN and most probably WILL break your savegame if modified.

local function create_id_table(_indexes)
    local _count, _table = 0, {}
    for k, v in pairs(_indexes) do
        _count = _count + 1
        _table[v] = _count
    end
    return _table
end


--[[--
Holds constant data that is shared between modules

@module const
]]
const = {
    --- Maximum unsigned integer value, used for random seeding in mapgensettings.
    max_uint = 4294967295,
    --- Surfaces data, stores various constant data for surfaces, such as: prefix (<code>const.surface.prefix</code>),
    -- relative locations (<code>const.surface.rel\_loc</code>), surface types (<code>const.surface.type</code>),
    -- tiles to overriden during chunk corrections (<code>const.surface.override\_tiles</code>),
    -- and the surface separator string (<code>const.surface.separator</code>) 
    surface = {
        -- Every surface created by this mod will be prefixed with this tag, it must not be changed or saves will be broken.
        prefix = "surfacesmod",
        -- Separator string, used to construct surfaces, if this is changed, surfaces will need to be migrated.
        separator = "-",
        -- Tiles that will be overridden during Surfaces chunk corrections
        override_tiles = table.reverse({
            --Vanilla tiles
            "out-of-map","deepwater","deepwater-green","water","water-green","grass-1","grass-2","grass-3","grass-4","dry-dirt","dirt-1","dirt-2","dirt-3","dirt-4","dirt-5","dirt-6","dirt-7","sand-1","sand-2","sand-3","red-desert-0","red-desert-1","red-desert-2","red-desert-3",

            --Alien Biomes Tiles
            --
            "frozen-snow-0",
            "frozen-snow-1",
            "frozen-snow-2",
            "frozen-snow-3",
            "frozen-snow-4",
            "frozen-snow-5",
            "frozen-snow-6",
            "frozen-snow-7",
            "frozen-snow-8",
            "frozen-snow-9",
            "mineral-aubergine-dirt-1",
            "mineral-aubergine-dirt-2",
            "mineral-aubergine-dirt-3",
            "mineral-aubergine-dirt-4",
            "mineral-aubergine-dirt-5",
            "mineral-aubergine-dirt-6",
            "mineral-aubergine-sand-1",
            "mineral-aubergine-sand-2",
            "mineral-aubergine-sand-3",
            "mineral-beige-dirt-1",
            "mineral-beige-dirt-2",
            "mineral-beige-dirt-3",
            "mineral-beige-dirt-4",
            "mineral-beige-dirt-5",
            "mineral-beige-dirt-6",
            "mineral-beige-sand-1",
            "mineral-beige-sand-2",
            "mineral-beige-sand-3",
            "mineral-black-dirt-1",
            "mineral-black-dirt-2",
            "mineral-black-dirt-3",
            "mineral-black-dirt-4",
            "mineral-black-dirt-5",
            "mineral-black-dirt-6",
            "mineral-black-sand-1",
            "mineral-black-sand-2",
            "mineral-black-sand-3",
            "mineral-brown-dirt-1",
            "mineral-brown-dirt-2",
            "mineral-brown-dirt-3",
            "mineral-brown-dirt-4",
            "mineral-brown-dirt-5",
            "mineral-brown-dirt-6",
            "mineral-brown-sand-1",
            "mineral-brown-sand-2",
            "mineral-brown-sand-3",
            "mineral-cream-dirt-1",
            "mineral-cream-dirt-2",
            "mineral-cream-dirt-3",
            "mineral-cream-dirt-4",
            "mineral-cream-dirt-5",
            "mineral-cream-dirt-6",
            "mineral-cream-sand-1",
            "mineral-cream-sand-2",
            "mineral-cream-sand-3",
            "mineral-dustyrose-dirt-1",
            "mineral-dustyrose-dirt-2",
            "mineral-dustyrose-dirt-3",
            "mineral-dustyrose-dirt-4",
            "mineral-dustyrose-dirt-5",
            "mineral-dustyrose-dirt-6",
            "mineral-dustyrose-sand-1",
            "mineral-dustyrose-sand-2",
            "mineral-dustyrose-sand-3",
            "mineral-grey-dirt-1",
            "mineral-grey-dirt-2",
            "mineral-grey-dirt-3",
            "mineral-grey-dirt-4",
            "mineral-grey-dirt-5",
            "mineral-grey-dirt-6",
            "mineral-grey-sand-1",
            "mineral-grey-sand-2",
            "mineral-grey-sand-3",
            "mineral-purple-dirt-1",
            "mineral-purple-dirt-2",
            "mineral-purple-dirt-3",
            "mineral-purple-dirt-4",
            "mineral-purple-dirt-5",
            "mineral-purple-dirt-6",
            "mineral-purple-sand-1",
            "mineral-purple-sand-2",
            "mineral-purple-sand-3",
            "mineral-red-dirt-1",
            "mineral-red-dirt-2",
            "mineral-red-dirt-3",
            "mineral-red-dirt-4",
            "mineral-red-dirt-5",
            "mineral-red-dirt-6",
            "mineral-red-sand-1",
            "mineral-red-sand-2",
            "mineral-red-sand-3",
            "mineral-tan-dirt-1",
            "mineral-tan-dirt-2",
            "mineral-tan-dirt-3",
            "mineral-tan-dirt-4",
            "mineral-tan-dirt-5",
            "mineral-tan-dirt-6",
            "mineral-tan-sand-1",
            "mineral-tan-sand-2",
            "mineral-tan-sand-3",
            "mineral-violet-dirt-1",
            "mineral-violet-dirt-2",
            "mineral-violet-dirt-3",
            "mineral-violet-dirt-4",
            "mineral-violet-dirt-5",
            "mineral-violet-dirt-6",
            "mineral-violet-sand-1",
            "mineral-violet-sand-2",
            "mineral-violet-sand-3",
            "mineral-white-dirt-1",
            "mineral-white-dirt-2",
            "mineral-white-dirt-3",
            "mineral-white-dirt-4",
            "mineral-white-dirt-5",
            "mineral-white-dirt-6",
            "mineral-white-sand-1",
            "mineral-white-sand-2",
            "mineral-white-sand-3",
            "vegetation-blue-grass-1",
            "vegetation-blue-grass-2",
            "vegetation-green-grass-1",
            "vegetation-green-grass-2",
            "vegetation-green-grass-3",
            "vegetation-green-grass-4",
            "vegetation-mauve-grass-1",
            "vegetation-mauve-grass-2",
            "vegetation-olive-grass-1",
            "vegetation-olive-grass-2",
            "vegetation-orange-grass-1",
            "vegetation-orange-grass-2",
            "vegetation-purple-grass-1",
            "vegetation-purple-grass-2",
            "vegetation-red-grass-1",
            "vegetation-red-grass-2",
            "vegetation-turquoise-grass-1",
            "vegetation-turquoise-grass-2",
            "vegetation-violet-grass-1",
            "vegetation-violet-grass-2",
            "vegetation-yellow-grass-1",
            "vegetation-yellow-grass-2",
            "volcanic-blue-heat-1",
            "volcanic-blue-heat-2",
            "volcanic-blue-heat-3",
            "volcanic-blue-heat-4",
            "volcanic-green-heat-1",
            "volcanic-green-heat-2",
            "volcanic-green-heat-3",
            "volcanic-green-heat-4",
            "volcanic-orange-heat-1",
            "volcanic-orange-heat-2",
            "volcanic-orange-heat-3",
            "volcanic-orange-heat-4",
            "volcanic-purple-heat-1",
            "volcanic-purple-heat-2",
            "volcanic-purple-heat-3",
            "volcanic-purple-heat-4"

            --Append Other mod tiles here

        }),
        -- Surface type classification, the surfaces module uses this information to identify surfaces from this mod. Each entry contains both an ID and a name.
        type = {
            underground = {
                id = 1,
                name = "cavern"
            },
            sky = {
                id = 2,
                name = "platform"
            },
            all = {
                id = 3,
                name = "all"
            }
        },
        -- Relative location, used in surfaces module and pairutil module for dimensional logic.
        rel_loc = create_id_table({"above", "below"})
    },
    -- Event Manager data
    eventmgr = {
        -- Task definitions, used to uniquely identify each "task" in the event manager
        task = create_id_table({"trigger_create_paired_entity", "trigger_create_paired_surface", "create_paired_entity", 
        "finalize_paired_entity", "remove_sky_tile", "spill_entity_result"}),
        -- Handle definitions, these are used to call functions from within the on_tick timer in the events module
        handle = {
            -- Used to teleport players between surfaces once configured waiting time has passed, see config.lua
            access_shaft_teleportation = {
                id = 1,
                tick = 5,
                func = "update_players_using_access_shafts"
            },
            -- Used to check each player for a nearby access shaft, prior to teleportation.
            access_shaft_update = {
                id = 2,
                tick = 5,
                func = "check_player_collision_with_access_shafts"
            },
            -- Used to update transport chest contents, moves items from transport chests to receiver chests
            item_transport_update = {
                id = 3,
                tick = 5,
                func = "update_transport_chest_contents"
            },
            -- Used to update intersurface fluid tank contents
            fluid_transport_update = {
                id = 4,
                tick = 10,
                func = "update_fluid_transport_contents"
            },
            -- Used to update intersurface accumulator contents
            energy_transport_update = {
                id = 4,
                tick = 10,
                func = "update_energy_transport_contents"
            },
            -- Used to execute the first task in the Task Manager queue
            taskmgr_update = {
                id = 6,
                tick = 2,
                func = "execute_first_task_in_waiting_queue"
            },
            -- Inspects the global surface migrations table for entries, processing in a serial manner, similar to the task queue
            surface_migrations_update = {
                id = 7,
                tick = 30,
                func = "surfaces_migrations"
            }
        }
    },
    -- Wire definitions, used to determine which wires to connect when placing paired entities
    wire = create_id_table({"copper", "red", "green", "circuit", "all"}),
    -- Tiers available for transport chests, will also be used for other purposes in the future
    tier = create_id_table({"crude", "basic", "standard", "improved", "advanced"})
}

-- index table for <code>const.eventmgr.task</code>
const.eventmgr.task_valid = table.reverse(const.eventmgr.task, true)
-- index table for <code>const.tier</code>
const.tier_valid = table.reverse(const.tier, true)
-- index table for <code>const.surface.type</code>
const.surface.type_valid = table.reverse(const.surface.type, true, "id")
-- index table for <code>const.surface.rel_loc</code>
const.surface.rel_loc_valid = table.reverse(const.surface.rel_loc, true)

return const
