data:extend({

	{
		type = "technology",
		name = "Deep-digging",
		icon = "__Surfaces_Reloaded__/graphics/icons/underground-entrance.png",
		effects = {
			{
				type = "unlock-recipe",
				recipe = "underground_entrance"
			},
			{
				type = "unlock-recipe",
				recipe = "underground_exit"
			},
			{
				type = "unlock-recipe",
				recipe = "crude_connector"
			},
			{
				type = "unlock-recipe",
				recipe = "basic_connector"
			},
			{
				type = "unlock-recipe",
				recipe = "standard_connector"
			},
			{
				type = "unlock-recipe",
				recipe = "improved_connector"
			},
			{
				type = "unlock-recipe",
				recipe = "advanced_connector"
			},
			{
				type = "unlock-recipe",
				recipe = "crude_servo"
			},
			{
				type = "unlock-recipe",
				recipe = "standard_servo"
			},
			{
				type = "unlock-recipe",
				recipe = "improved_servo"
			}
			
		},
		prerequisites={
			"steel-processing"
		},
		unit={
			count = 100,
			ingredients={
				{"automation-science-pack", 1}
			},
			time = 100
		},
	},
		{
		type = "technology",
		name = "to_the_sky",
		icon = "__Surfaces_Reloaded__/graphics/icons/sky-entrance.png",
		effects = {
			{
				type = "unlock-recipe",
				recipe = "sky_entrance"
			},
			{
				type = "unlock-recipe",
				recipe = "wooden_floor"
			},
			{
				type = "unlock-recipe",
				recipe = "sky_exit"
			}
		},
		prerequisites={
			"steel-processing",
			"Deep-digging"
		},
		unit={
			count = 100,
			ingredients={
				{"automation-science-pack", 1}
			},
			time = 100
		},
	},
		{
		type = "technology",
		name = "Energy_transport",
		icon = "__Surfaces_Reloaded__/graphics/icons/energy-transport-upper.png",
		effects = {
			{
				type = "unlock-recipe",
				recipe = "energy_transport_lower"
			},
			{
				type = "unlock-recipe",
				recipe = "energy_transport_upper"
			},
						{
				type = "unlock-recipe",
				recipe = "energy_transport_2_lower"
			},
						{
				type = "unlock-recipe",
				recipe = "energy_transport_2_upper"
			},
						
						{
				type = "unlock-recipe",
				recipe = "energy_transport_3_lower"
			},
						
						{
				type = "unlock-recipe",
				recipe = "energy_transport_3_upper"
			},
						
						{
				type = "unlock-recipe",
				recipe = "energy_transport_4_lower"
			},
						
						{
				type = "unlock-recipe",
				recipe = "energy_transport_4_upper"
			}
			
		},
		prerequisites={
			"steel-processing",
			"electric-energy-accumulators-1",
			"Deep-digging"
		},
		unit={
			count = 100,
			ingredients={
				{"automation-science-pack", 1}
			},
			time = 100
		},
	},
		{
		type = "technology",
		name = "Item_transport",
		icon = "__Surfaces_Reloaded__/graphics/icons/iron-transport-chest-down.png",
		effects = {
			{
				type = "unlock-recipe",
				recipe = "transport_storehouse_up"
			},
						{
				type = "unlock-recipe",
				recipe = "transport_storehouse_down"
			},
						{
				type = "unlock-recipe",
				recipe = "logistic_transport_storehouse_down"
			},
			
						{
				type = "unlock-recipe",
				recipe = "logistic_transport_storehouse_up"
			},
						{
				type = "unlock-recipe",
				recipe = "transport_warehouse-up"
			},
						{
				type = "unlock-recipe",
				recipe = "transport_warehouse_down"
			},
						{
				type = "unlock-recipe",
				recipe = "logistic_transport_warehouse-up"
			},
			{
				type = "unlock-recipe",
				recipe = "logistic_transport_warehouse_down"
			},
			{
				type = "unlock-recipe",
				recipe = "receiver_storehouse_upper"
			},
			{
				type = "unlock-recipe",
				recipe = "receiver-storehouse-lower"
			},
			{
				type = "unlock-recipe",
				recipe = "logistic_receiver_storehouse_upper"
			},
			{
				type = "unlock-recipe",
				recipe = "logistic_receiver_storehouse_lower"
			},
			{
				type = "unlock-recipe",
				recipe = "receiver_warehouse_upper"
			},
						{
				type = "unlock-recipe",
				recipe = "receiver_warehouse_lower"
			},
						{
				type = "unlock-recipe",
				recipe = "logistic_receiver_warehouse_upper"
			},
						{
				type = "unlock-recipe",
				recipe = "logistic_receiver_warehouse_lower"
			},
						{
				type = "unlock-recipe",
				recipe = "wooden_transport_chest_up"
			},
						{
				type = "unlock-recipe",
				recipe = "wooden_transport_chest_down"
			},
						{
				type = "unlock-recipe",
				recipe = "iron_transport_chest_up"
			},
			{
				type = "unlock-recipe",
				recipe = "iron_transport_chest_down"
			},
			{
				type = "unlock-recipe",
				recipe = "steel_transport_chest_up"
			},
			{
				type = "unlock-recipe",
				recipe = "steel_transport_chest_down"
			}			
		},
		prerequisites={
			"steel-processing",
			"Deep-digging"
		},
		unit={
			count = 100,
			ingredients={
				{"automation-science-pack", 1}
			},
			time = 100
		},
	},
			{
		type = "technology",
		name = "Fluid_transport",
		icon = "__Surfaces_Reloaded__/graphics/icons/fluid-transport-upper.png",
		effects = {
			{
				type = "unlock-recipe",
				recipe = "fluid_transport_lower"
			},
			{
				type = "unlock-recipe",
				recipe = "fluid_transport_upper"
			},
			{
				type = "unlock-recipe",
				recipe = "fluid_transport_2_lower"
			},
			{
				type = "unlock-recipe",
				recipe = "fluid_transport_2_upper"
			},
			{
				type = "unlock-recipe",
				recipe = "fluid_transport_3_lower"
			},
			{
				type = "unlock-recipe",
				recipe = "fluid_transport_3_upper"
			},
			{
				type = "unlock-recipe",
				recipe = "fluid_transport_4_lower"
			},
			{
				type = "unlock-recipe",
				recipe = "fluid_transport_4_upper"
			}
		},
		prerequisites={
			"steel-processing",
			"fluid-handling",
			"Deep-digging"
		},
		unit={
			count = 100,
			ingredients={
				{"automation-science-pack", 1}
			},
			time = 100
		},
	},
})
